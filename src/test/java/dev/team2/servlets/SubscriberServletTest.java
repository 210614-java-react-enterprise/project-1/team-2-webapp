package dev.team2.servlets;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SubscriberServletTest {
    //GetSubscribersServlet
    private SubscriberServlet servlet;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Before
    public void setUp() throws SQLException {
        servlet = new SubscriberServlet();
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
    }

    //Set environment variables
    //@Test(expected = NullPointerException.class)
    @Test
    public void testDoPostAlreadyExists() throws ServletException, IOException {
        String requestBody = "{\"email\":\"t@g.com\",\"lastName\":\"Greenman\",\"firstName\":\"Trent\"," +
                "\"address\":\"1234 Road\",\"phone\":\"1112223334\",\"password\":\"P455W0RD\"}";

        request.setMethod("POST");
        request.setContent(requestBody.getBytes("UTF-8"));
        servlet.doPost(request, response);
    }

    //Set environment variables
    @Test
    public void testDoPostGood() throws ServletException, IOException {
        String requestBody = "{\"email\":\"daniel@dl.com\",\"lastName\":\"Lewis\",\"firstName\":\"DanielDay\"," +
                "\"address\":\"1234 Hollywood\",\"phone\":\"1112223334\",\"password\":\"P455W0RD\"}";

        request.setMethod("POST");
        request.setContent(requestBody.getBytes("UTF-8"));
        servlet.doPost(request, response);

    }

    @Test
    public void testDoDeleteGood() throws IOException, ServletException {
        String requestBody = "{\"email\":\"daniel@dl.com\",\"lastName\":\"Lewis\",\"firstName\":\"DanielDay\"," +
                "\"address\":\"1234 Hollywood\",\"phone\":\"1112223334\",\"password\":\"P455W0RD\"}";

        request.setMethod("DELETE");
        request.setContent(requestBody.getBytes("UTF-8"));
        servlet.doDelete(request, response);
    }

    @Test
    public void testDoDeleteBad() throws IOException, ServletException {
        String requestBody = "{\"email\":\"dsafdf@sadfads.com\",\"lastName\":\"sdfasa\",\"firstName\":\"dsafds\"," +
                "\"address\":\"1234 asdfas\",\"phone\":\"1112223333\",\"password\":\"sdaf\"}";

        request.setMethod("DELETE");
        request.setContent(requestBody.getBytes("UTF-8"));
        servlet.doDelete(request, response);
    }

    //FIGURE OUT HOW TO ADD MULTIPLE LINES TO BODY or REFACTOR DO PUT
    /*
    @Test
    public void testDoPutBad() throws IOException, ServletException {
        String requestBodyPost = "{\"email\":\"trent@greenman.com\",\"lastName\":\"Greenman\",\"firstName\":\"Trent\"," +
                "\"birthDate\":[1999,1,22],\"address\":\"1234 Road\",\"phone\":\"1112223333\",\"password\":\"P455W0RD\"}";

        String requestBodyPut = "[{\"email\":\"trent@greenman.com\",\"lastName\":\"Greenman\",\"firstName\":\"Trent\"," +
                "\"birthDate\":[1999,1,22],\"address\":\"1234 Road\",\"phone\":\"1112223333\",\"password\":\"P455W0RD\"}," +
                "{\"password\":\"1234\"}]";

        request.setMethod("POST");
        request.setContent(requestBodyPost.getBytes("UTF-8"));
        servlet.doPost(request, response);

        request.setMethod("PUT");
        request.setContent(requestBodyPut.getBytes("UTF-8"));
        servlet.doPut(request, response);
    }
     */
}
