package dev.team2.util;

//import com.sun.org.apache.xalan.internal.xslt.Process;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;

public class TempConnectionUtilTest {
    //TempConnectionUtil
    //Set environment variables for this
    @Test
    public void testGoodGetConnection() throws SQLException {
        //ProcessBuilder builder = new ProcessBuilder();
        assertNotNull(TempConnectionUtil.getConnection());
    }

    //Set environment variables to incorrect PASSWORD/USERNAME
    @Test(expected = SQLException.class)
    public void testBadGetConnection() throws SQLException {
        TempConnectionUtil.getConnection();
    }
}
