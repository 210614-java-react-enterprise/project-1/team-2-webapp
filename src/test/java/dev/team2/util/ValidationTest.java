package dev.team2.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidationTest {

    //Validation
    Validation validation = new Validation();
    @Test
    public void testGoodPhone10(){
        assertTrue(validation.verifyPhone("1234567890"));
    }

    @Test
    public void testGoodPhone11(){
        assertTrue(validation.verifyPhone("12345678900"));
    }

    @Test
    public void testBadPhoneLength(){
        assertFalse(validation.verifyPhone("12345"));
    }

    @Test
    public void testBadPhoneCharacters(){
        assertFalse(validation.verifyPhone("R123456789"));
    }

    @Test
    public void testGoodEmail(){
        assertTrue(validation.verifyEmail("trent@revature.com"));
    }

    @Test
    public void testBadEmailNoAt(){
        assertFalse(validation.verifyEmail("trentrevature.com"));
    }

    @Test
    public void testBadEmailAtEnd(){
        assertFalse(validation.verifyEmail("@trentrevature.com"));
    }

    @Test
    public void testBadEmailSpecial(){
        assertFalse(validation.verifyEmail("(*trent*)@revature.com"));
    }
}
