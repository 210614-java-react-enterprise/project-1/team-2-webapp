select * from email_table_ e inner join identity_table_ i on e.emailid = i.emailid;  
select * from email_table_;
select * from identity_table_;
select * from password_table_;
 

 /* _        _     _ */
/* | |_ __ _| |__ | | ___  ___ */
/* | __/ _` | '_ \| |/ _ \/ __| */
/* | || (_| | |_) | |  __/\__ \ */
 /* \__\__,_|_.__/|_|\___||___/ */

/*^*/
create table email_table_test_ (
	emailId UUID primary key default gen_random_uuid(),
	email text not null unique
);

create table identity_table_test_ (
    emailId UUID references email_table_test_ on delete cascade not null unique,
	identityId UUID primary key default gen_random_uuid(),
    lastName text not null,
    firstName text not null,
    birthDate date not null,
    address text not null,
    phone text not null
);

create table password_table_test_ (
    emailId UUID references email_table_test_ on delete cascade not null unique,
	passwordId UUID primary key default gen_random_uuid(),
    password text not null
);

create table status_table_ (
	emailId UUID references email_table_ on delete cascade not null unique,
	isAdmin boolean default false
);

create table status_table_test_ (
	emailId UUID references email_table_test_ on delete cascade not null unique,
	isAdmin boolean default false
);
 

select * from email_table_test_;
select * from identity_table_test_;
select * from password_table_test_;

truncate table email_table_test_;
truncate table identity_table_test_;
truncate table password_table_test_; 
